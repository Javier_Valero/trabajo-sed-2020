-- Code your design here
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;


entity FPGA4bit is
  generic (
    WIDTH : positive := 4
  );
port(
	entrada1: in std_logic;
    entrada2: in std_logic;
    entrada3: in std_logic;
    entrada4: in std_logic;
    salida1: out std_logic;
	salida2: out std_logic;
    salida3: out std_logic;
    salida4: out std_logic;
    CLK: in std_logic;
    LED: out std_logic_vector (WIDTH-1 downto 0)
	);
end entity;

--El programa devuelve lo que lee, seg�n el valor de entrada pondr� 
--un numero en el bloque BCD de la salida.
--Se usara funcion decodificador.

architecture behavioral of FPGA4bit is
	signal entrada: std_logic_vector(WIDTH-1 downto 0);
begin
	process(CLK)
    	begin
    	if CLK'event and CLK='0' then --funcionamos a flanco negativo
        	entrada(0) <= entrada1;
            entrada(1) <= entrada2;
            entrada(2) <= entrada3;
            entrada(3) <= entrada4;
            salida1 <= entrada(0);
            salida2 <= entrada(1);
            salida3 <= entrada(2);
            salida4 <= entrada(3);
        end if;
    end process;
    LED <= entrada;
end architecture;