library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.ALL;

ENTITY top IS
 PORT ( 
 entrada1 : IN std_logic;
 entrada2 : IN std_logic;
 entrada3 : IN std_logic;
 entrada4 : IN std_logic;
 salida1  : OUT std_logic;
 salida2  : OUT std_logic;
 salida3  : OUT std_logic;
 salida4  : OUT std_logic;
 digsel   : IN std_logic_vector(3 DOWNTO 0);
 digctrl  : OUT std_logic_vector(3 DOWNTO 0);
 segment  : OUT std_logic_vector(6 DOWNTO 0);
 CLK : IN STD_LOGIC
 );
END top;
architecture structural of top is
 SIGNAL s : std_logic_vector(6 downto 0);
 signal code: std_logic_vector (3 downto 0);
 
 COMPONENT FPGA4bit is
  generic (
    WIDTH : positive := 4
  );
port(
	entrada1: in std_logic;
    entrada2: in std_logic;
    entrada3: in std_logic;
    entrada4: in std_logic;
    salida1: out std_logic;
	salida2: out std_logic;
    salida3: out std_logic;
    salida4: out std_logic;
    CLK: in std_logic;
    LED: out std_logic_vector (WIDTH-1 downto 0)
	);
end COMPONENT;
COMPONENT decoder
PORT (
code : IN std_logic_vector(3 DOWNTO 0);
led  : OUT std_logic_vector(6 DOWNTO 0)
);
END COMPONENT;

begin
Inst_FPGA4bit: FPGA4bit PORT MAP(
   entrada1 => entrada1,
   entrada2 => entrada2,
   entrada3 => entrada3,
   entrada4 => entrada4,
   salida1  => salida1,
   salida2  => salida2,
   salida3  => salida3,
   salida4  => salida4,
   CLK => CLK,
   LED => code
);
Inst_decoder: decoder PORT MAP (
    code => code,
    led  => s
);
    digctrl <= not  digsel;
    segment <=  s;
end architecture structural;
