-- Code your design here
library IEEE;
use IEEE.std_logic_1164.all;

entity FPGA1bit is
port(
	entrada: in std_logic;
    salida: out std_logic;
    PEPE: in std_logic;
    LED: out std_logic
);
end entity;

--El programa devuelve 1 si lee un bit positivo a la entrada y enciende un led, sino 0.

architecture behavioral of FPGA1bit is
begin
	process(PEPE)
    	begin
    	if PEPE'event and PEPE='0' then --funcionamos a flanco negativo
        	salida<=entrada;
            if entrada = '1' then
            	LED<='1';
            elsif entrada='0' then
            	LED<='0';
            end if;
        end if;
    end process;
end architecture;
