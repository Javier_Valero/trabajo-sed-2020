library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity FPGA4bit_tb is
end FPGA4bit_tb;

architecture test of FPGA4bit_tb is
-- Constants
    constant BUS_WIDTH : positive := 4;
    constant CLK_PERIOD : time := 1 sec / 100_000_000; -- 10ns 
-- Inputs
    signal  CLK, entrada1, entrada2, entrada3, entrada4 : std_logic;
-- Outputs
    signal salida1, salida2, salida3, salida4 : std_logic;
    signal LED : std_logic_vector(BUS_WIDTH -1 downto 0);
    
    component FPGA4bit is
      generic (
        WIDTH : positive 
      );
      port(
	    entrada1: in std_logic;
        entrada2: in std_logic;
        entrada3: in std_logic;
        entrada4: in std_logic;
        salida1: out std_logic;
	    salida2: out std_logic;
        salida3: out std_logic;
        salida4: out std_logic;
        CLK: in std_logic;
        LED: out std_logic_vector (WIDTH-1 downto 0)
	  );
    end component;
    
begin
    --Unit Under Test
    uut: FPGA4bit
    generic map(
        WIDTH => BUS_WIDTH
      )
      port map(
	    entrada1 => entrada1,
        entrada2 => entrada2,
        entrada3 => entrada3,
        entrada4 => entrada4,
        salida1  => salida1,
	    salida2  => salida2,
        salida3  => salida3,
        salida4  => salida4,
        CLK  => CLK,
        LED  => LED
	  );
	  
 clk_gen : process  -- Trabajamos con flanco negativo
 begin
    CLK <= '1'; 
    wait for 0.5 * CLK_PERIOD;
    CLK <= '0';
    wait for 0.5 * CLK_PERIOD;
 end process;
 
 tester : process
 begin
 wait for 0.25 * CLK_PERIOD;
 -- Bus de entrada de datos
 entrada4 <= '0';
 entrada3 <= '0';
 entrada2 <= '0';
 entrada1 <= '0';
 
 wait until CLK = '0'; -- Esperamos a flanco negativo para que se actualicen las entradas
 wait for 0.5 * CLK_PERIOD;
 entrada4 <= '0';
 entrada3 <= '1';
 entrada2 <= '1';
 entrada1 <= '0';      
 
 wait until CLK = '0'; -- En el siguiente flanco negativo, se actualizan las salidas.
 wait for 0.1 * CLK_PERIOD;
 assert LED = X"6"
 report "[FAILURE]FPGA4bit Malfunction"
 severity failure;
 
 wait until CLK = '0'; 
 wait for 0.5 * CLK_PERIOD;
 entrada4 <= '1';      
 entrada3 <= '1';
 entrada2 <= '1';
 entrada1 <= '0';      
 wait until CLK = '0';
 
 
 -- Esperamos 8 ciclos de reloj
 for i in 1 to 3 loop
    wait until CLK = '0';
 end loop;
 
 -- Fin de simulación exitoso
 wait for 0.25 * CLK_PERIOD;
 assert false
 report "[SUCCESS] Simulation Finished OK"
 severity failure;
 
 end process;

end architecture;
