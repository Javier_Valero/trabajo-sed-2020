-- Code your testbench here
library IEEE;
use IEEE.std_logic_1164.all;

entity FPGA1bit_tb is
end entity;

architecture test of FPGA1bit_tb is
	signal entrada,salida, CLK, LED: std_logic;
    
	component FPGA1bit is
	port(
	entrada: in std_logic;
    salida: out std_logic;
    CLK: in std_logic;
    LED: out std_logic
	);
	end component;
    
    constant CLK_PERIOD : time     := 10 ns;  -- Clock period

begin
  -- Unit Under Test
  uut: FPGA1bit
  port map(
  entrada=>entrada,
  salida=>salida,
  CLK=>CLK,
  LED=>LED
  );
  
   clkgen: process
  begin
    CLK <= '1';
    wait for 0.5 * CLK_PERIOD;
    CLK <= '0';
    wait for 0.5 * CLK_PERIOD;
  end process;
  
  tester: process
  begin
    -- Test reset
    wait for 0.25 * CLK_PERIOD;
    entrada <= '1';
    wait for 1.50 * CLK_PERIOD;
    entrada <= '0';
    
    for i in 1 to 4 loop
        wait until CLK = '0';   -- Reloj por flanco negativo
    end loop;
    
    wait for 0.25 * CLK_PERIOD;
    assert false
    report "[SUCESS] Simulation Finished OK"
    severity failure;
    
  end process;
  
end architecture;