-- Code your design here
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;

entity servomotor is
port(
	PWM: out std_logic;
    MODE: in std_logic --1 180 grados, 0 0 grados
);
end entity;

architecture mov of servomotor is
signal CLK: std_logic;
constant CLK_PERIOD:time:= 20 ms; --20ms
	begin
	process
    begin
    	CLK<='0';
        	wait for 0.5*CLK_PERIOD;
        CLK<='1';
        	wait for 0.5*CLK_PERIOD;
    end process;
    process
    begin
    	if CLK'event and CLK='1' then
        	if MODE='0' then
            	PWM<='1';
                	wait for CLK_PERIOD*0.05; --1ms
                PWM<='0';
             elsif MODE='1' then
             	PWM<='1';
                wait for CLK_PERIOD*0.1; --2ms
                PWM<='0';
            end if;
          end if;
    end process;

end architecture;