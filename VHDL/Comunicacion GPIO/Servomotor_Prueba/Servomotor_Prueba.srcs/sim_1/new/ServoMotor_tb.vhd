-- Code your testbench here
library IEEE;
use IEEE.std_logic_1164.all;

entity servomotor_tb is
end entity;


ARCHITECTURE TEST OF servomotor_tb is
signal MODE:std_logic:='0';
signal PWM: std_logic;
component servomotor is
	port(
		PWM: out std_logic;
    	MODE: in std_logic --1 180 grados, 0 0 grados
	);
	end component;
	constant TIM: time:= 1 sec;
begin
	uut: servomotor
    	port map(
        	PWM=>PWM,
            MODE=>MODE
        );
	MODE<=not MODE after TIM;
    
    process
    begin
    	
    	wait for 5*TIM ;
    assert false
      report "[SUCCESS]: simulation finished."
      severity failure;
    end process;
end architecture;
