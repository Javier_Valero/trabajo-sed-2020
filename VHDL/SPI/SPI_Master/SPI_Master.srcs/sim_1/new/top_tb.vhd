-- Code your testbench here
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.NUMERIC_sTD.all;

entity top_tb is
end entity;

architecture test of top_tb is
	constant FREC_NEXUS : time := 1 sec /100_000_000;
    --input
	signal CLK_FPGA,CE,MISO: std_logic; 
    --output
    signal CLK,MOSI,CS: std_logic;
    signal segment  : std_logic_vector(6 DOWNTO 0);
    
    
   component top IS
 PORT ( 
    CLK: out std_logic;
    CLK_FPGA: in std_logic; --reloj de la FPGA a 50 MHz
    CE: in std_logic; --Communication Enable 
    MOSI: out std_logic; --datos que van al esclavo
    MISO: in std_logic; -- datos que vienen del master
    CS: out std_logic; --Chip select activo a nivel bajo.
    segment  : OUT std_logic_vector(6 DOWNTO 0)
    );
    END component;
begin
	uut:top
    	port map(
        CLK => CLK,
        CLK_FPGA => CLK_FPGA,
        CE => CE,
        MOSI => MOSI,
        MISO => MISO,
        CS => CS,
        segment=>segment  
        );
        
        
	clk_decl:process
    	begin
        CLK_FPGA<='0';
        wait for FREC_NEXUS *0.5;
        CLK_FPGA <='1';
        wait for FREC_NEXUS*0.5;
        end process;
       
	ce_decl:process
    begin
    	CE<='1';
        wait until CS='0';
        CE<='0';
        wait until CS='1';
        wait for 15 * FREC_NEXUS;
    end process;
	
    MISO_decl: process
        BEGIN
    	MISO <= '1';
        wait until MOSI='1';
        MISO<='0';
        wait until MOSI='1';
    end process;
    
    fail: process
        BEGIN
        wait until cs='0';
        wait until cs ='1';
        wait until cs='0';
    	wait until cs='1';
        assert false
  	report "[SUCESS]"
  	severity failure;
  	end process;
end architecture;