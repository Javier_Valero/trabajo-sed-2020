library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.ALL;

ENTITY top IS
 PORT ( 
 CLK: out std_logic;
 CLK_FPGA: in std_logic; --reloj de la FPGA a 50 MHz
  CE: in std_logic; --Communication Enable 
  MOSI: out std_logic; --datos que van al esclavo
  MISO: in std_logic; -- datos que vienen del master
 CS: out std_logic; --Chip select activo a nivel bajo.
 digsel   : IN std_logic_vector(3 DOWNTO 0):="0000";
 digctrl  : OUT std_logic_vector(3 DOWNTO 0):="0000";
 segment  : OUT std_logic_vector(6 DOWNTO 0)
 );
END top;
architecture structural of top is
 SIGNAL s : std_logic_vector(6 downto 0);
 signal code: std_logic_vector (7 downto 0);
 
 COMPONENT SPI_MASTER is
  generic (
    CLK_NEXUS: positive:= 50_000_000; --frecuencia de la placa
    SCLK:positive := 400_000	--frecuencia maxima de salida del CLK
  );
port(
	CLK: out std_logic;
    CLK_FPGA: in std_logic; --reloj de la FPGA a 50 MHz
    CE: in std_logic; --Communication Enable 
    MOSI: out std_logic; --datos que van al esclavo
    MISO: in std_logic; -- datos que vienen del master
    datos_leidos: out std_logic_vector (7 downto 0);
    --datos_escribir: in std_logic_vector (7 downto 0); -- en el codigo voy a hacer que se generen un pocos bits y tirando
    CS: out std_logic --Chip select activo a nivel bajo.
);
end COMPONENT;
COMPONENT decoder
PORT (
code : IN std_logic_vector(3 DOWNTO 0);
led  : OUT std_logic_vector(6 DOWNTO 0)
);
END COMPONENT;

begin
Inst_SPI: SPI_MASTER PORT MAP(
   CLK => CLK,
   CLK_FPGA => CLK_FPGA,
   CE => CE,
   MOSI => MOSI,
   MISO  => MISO,
   datos_leidos  => code,
   CS  => CS
);
Inst_decoder: decoder PORT MAP (
    code => code(3 downto 0),
    led  => s
);
    digctrl <= not  digsel;
    segment <=  s;
end architecture structural;
