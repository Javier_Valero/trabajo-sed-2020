-- Code your design here
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.NUMERIC_sTD.all;

entity SPI_MASTER is
generic(
	CLK_NEXUS: positive:= 50_000_000; --frecuencia de la placa
    SCLK:positive := 400_000	--frecuencia maxima de salida del CLK
);
port(
	CLK: out std_logic;
    CLK_FPGA: in std_logic; --reloj de la FPGA a 50 MHz
    CE: in std_logic; --Communication Enable 
    MOSI: out std_logic; --datos que van al esclavo
    MISO: in std_logic; -- datos que vienen del master
    datos_leidos: out std_logic_vector (7 downto 0);
    --datos_escribir: in std_logic_vector (7 downto 0); -- en el codigo voy a hacer que se generen un pocos bits y tirando
    CS: out std_logic --Chip select activo a nivel bajo.
);
end entity;


architecture behavioral of SPI_MASTER is
-- declaramos estados
TYPE STATE IS (ESPERANDO,ENCIENDO,COMUNICANDO, APAGO);
signal estado: STATE;
signal cambio_estado: std_logic:='0'; --Marca el cambio del estado
signal counter_W: integer range 0 to 9:=0; --contador lectura
signal counter_R: integer range 0 to 7:=7; --contador escritura
signal CLK_counter1: integer range 0 to 2000000:=0;
signal CLK_counter2: integer range 0 to 2000000:=0;
signal data_to_send: std_logic_vector (7 downto 0):="11001011"; --mandamos 203
signal CLK_OUT: std_logic;
signal start_com: std_logic:='1';
begin
--cambios de estado
	process(cambio_estado, CE,CLK_FPGA)
    	begin
        if CE'event and CE='1' and estado=ESPERANDO then
       		estado<=ENCIENDO;
        end if;
        if cambio_estado'event and cambio_estado='0' and estado=APAGO then
        	estado<=ESPERANDO;
        end if;
        if cambio_estado'event and cambio_estado='1' then
        	case estado is
            	when ENCIENDO=> estado<=COMUNICANDO;
                when COMUNICANDO => estado<=APAGO;
                when others => estado<=estado;
            end case;
        end if;
     end process;
	
--tratamiento de los estados
	process(CLK_FPGA)
    begin
    	if rising_edge(CLK_FPGA) then
        	if estado=ESPERANDO then
            	CS<='1';
                CLK_OUT<='1';
            	cambio_estado<='0';
        	elsif estado=ENCIENDO then
            	CS<='0'; --chip select a 0, comienza comunicación
                CLK_OUT<='0';
                cambio_estado<='1';
            elsif estado=COMUNICANDO then --aqui empieza lo chido
            	if start_com='1' then
            	cambio_estado<='0';
                start_com<='0';
                end if;
                ----Contadores
                CLK_counter1 <= CLK_counter1 + 1;
                if CLK_counter1=2000 then
            	CLK_counter2 <= CLK_counter2 + 1;
                CLK_counter1 <= 0;
                end if;
                
            	if Counter_W = 9 then
                	cambio_estado<= '1';
           		 elsif CLK_counter2 = 1500 then --500ms/(16*20ns) --if se cumple cada flanco, por eso 16 y no 8
                 	CLK_counter2 <= 0; --reiniciamos el contador
                 	if CLK_OUT='0' then --cambiamos el valor del reloj y hacemos operacion
                    	if counter_W < 8 then
                    	CLK_OUT<='1';
                        end if;
                        --escritura
                        MOSI<=data_to_send(7);
                        data_to_send <= data_to_send(6 downto 0) & data_to_send(7);
                        counter_W<= counter_W + 1;
                        --lectura
                        datos_leidos(counter_R)<=MISO;
                        if counter_R > 0 then
                        counter_R<=counter_R - 1;
                        end if;
                    else CLK_OUT<='0';
                 	end if;	
           		 end if;
        	elsif estado=APAGO then
            	--cambio_estado<='0';
            	CS<= '1'; --ponemos el chip select a 1
                data_to_send <=data_to_send(0) & data_to_send(7 downto 1);
                counter_R<=7;
                CLK_OUT<='1';
                counter_W<=0;
                CLK_counter1<=0;
                CLK_counter2<=0;
                MOSI<='0';
                start_com<='1';
                cambio_estado <= '0';
        end if;
    end if;
    end process;
    CLK <= CLK_OUT;
end architecture;


